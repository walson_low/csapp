This repository serves as a tool for myself to learn how a machine works, by following the practical questions and applying them in general.  
Also helps with learning the markdown syntax.

[Markdown Math Syntax Cheatsheet](https://csrgxtu.github.io/2015/03/20/Writing-Mathematic-Fomulars-in-Markdown/)

# CS:APP Second Edition
Code snippets available at: [http://csapp.cs.cmu.edu/2e/code.html](http://csapp.cs.cmu.edu/2e/code.html)